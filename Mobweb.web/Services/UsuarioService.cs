﻿using Mobweb.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Mobweb.web.Helpers;
using Mobweb.web.Services;
using System.Net;
using Mobweb.web.DAL;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Mobweb.web.Services
{
    public class UsuarioService
    {
        private CMContext db = new CMContext();

        public List<Cliente> GetClientesByUserID(int id)
        {
            Usuario user = db.Usuarios.Find(id);
            if (user != null)
                return user.Clientes.ToList();
            else
                return null;
        } 

        public Usuario GetUsuarioByLogin(string login)
        {
            foreach (var item in db.Usuarios.ToList())
            {
                if (item.NomeUsuario.Equals(login))
                    return item;
            }
            return null;
        }

        public int CreateUsuario(Usuario usuario)
        {
            List<Usuario> usuarios = db.Usuarios.ToList();
            foreach(var item in usuarios)
            {
                if (item.NomeUsuario.Equals(usuario.NomeUsuario))
                {
                    //nome de usuario ja usado.
                    return 2;
                }else if (item.Email.Equals(usuario.Email))
                {
                    //email ja usado.
                    return 3;
                }
            }
            usuario.Senha = CriptoHelper.GetEncripted(usuario.Senha);
            db.Usuarios.Add(usuario);
            db.SaveChanges();
            return 1;
        }

        public Usuario GetUsuarioByID(int? id)
        {
            return db.Usuarios.Find(id);
        }

        public Usuario GetLogin(ContaViewModel conta)
        {
            Usuario user = GetUsuarioByLogin(conta.UserName);
            if(user!=null && user.NomeUsuario.Equals(conta.UserName) && user.Senha.Equals(CriptoHelper.GetEncripted(conta.Senha))){
                return user;
            }
            else
            {
                return null;
            }

        }

        public void Update(Usuario usuario)
        {
                usuario.Senha = CriptoHelper.GetEncripted(usuario.Senha);
                db.Set<Usuario>().AddOrUpdate(usuario);
                db.SaveChanges();
        }
    }
}