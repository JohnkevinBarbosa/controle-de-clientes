﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Mobweb.web.DAL;
using Mobweb.web.Helpers;
using Mobweb.web.Models;


namespace Mobweb.web.Services
{
    
    public class ClienteService
    {
        private CMContext db = new CMContext();
        private UsuarioService userService = new UsuarioService();

        public int CreateClient(Cliente client)
        {
            
            foreach(var item in db.Clientes.ToList())
            {
                if(item.CNPJ.Equals(client.CNPJ) || item.RazaoSocial.Equals(client.RazaoSocial))
                {
                    return 1;
                }
            }

            db.Clientes.Add(client);
            db.SaveChanges();
            return 0;

        }

        public Cliente GetClientByID(int id)
        {
            return db.Clientes.Find(id);
        }
        public void Update(Cliente client)
        {
            db.Entry(client).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Remove(int ID)
        {
            Cliente client = db.Clientes.Find(ID);
            db.Clientes.Remove(client);
            db.SaveChanges();
        }

        public bool ValidarCNPJ(string cnpj)
        {
           bool isValid = MasterValidator.ValidarCNPJ(cnpj);
            if (isValid)
                return !ExistCNPJ(cnpj);
            else
                return false;
        }

        public bool ExistCNPJ(string cnpj)
        {
            foreach(var item in db.Clientes.ToList())
            {
                if (item.CNPJ.Equals(cnpj))
                    return true;
            }
            return false;
        }

        public bool ExistCNPJWithID(string cnpj,int id)
        {
            foreach (var item in db.Clientes.ToList())
            {
                if (item.CNPJ.Equals(cnpj) && item.ClienteID!=id)
                    return true;
            }
            return false;
        }
    }
}