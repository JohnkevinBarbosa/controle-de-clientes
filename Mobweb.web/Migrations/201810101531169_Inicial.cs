namespace Mobweb.web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usuario", "NomeUsuario", c => c.String(nullable: false));
            AlterColumn("dbo.Usuario", "Senha", c => c.String(nullable: false));
            AlterColumn("dbo.Usuario", "NomeCompleto", c => c.String(nullable: false));
            DropColumn("dbo.Usuario", "UsuarioLogin");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Usuario", "UsuarioLogin", c => c.String());
            AlterColumn("dbo.Usuario", "NomeCompleto", c => c.String());
            AlterColumn("dbo.Usuario", "Senha", c => c.String());
            DropColumn("dbo.Usuario", "NomeUsuario");
        }
    }
}
