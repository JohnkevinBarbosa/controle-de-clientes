namespace Mobweb.web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        ClienteID = c.Int(nullable: false, identity: true),
                        RazaoSocial = c.String(),
                        NomeFantasia = c.String(),
                        CNPJ = c.String(),
                        Logradouro = c.String(),
                        Numero = c.Int(nullable: false),
                        Bairro = c.String(),
                        Complemento = c.String(),
                        Municio = c.String(),
                        Cep = c.Int(nullable: false),
                        UsuarioID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClienteID)
                .ForeignKey("dbo.Usuario", t => t.UsuarioID, cascadeDelete: true)
                .Index(t => t.UsuarioID);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        UsuarioID = c.Int(nullable: false, identity: true),
                        UsuarioLogin = c.String(),
                        Senha = c.String(),
                        NomeCompleto = c.String(),
                        Email = c.String(),
                        Telefone = c.String(),
                    })
                .PrimaryKey(t => t.UsuarioID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cliente", "UsuarioID", "dbo.Usuario");
            DropIndex("dbo.Cliente", new[] { "UsuarioID" });
            DropTable("dbo.Usuario");
            DropTable("dbo.Cliente");
        }
    }
}
