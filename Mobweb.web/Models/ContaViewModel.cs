﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mobweb.web.Models
{
    public class ContaViewModel
    {
        [Required(ErrorMessage ="Informe seu Nome de Usuario.")]
        [Display(Name ="Usuario: ")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Digite sua senha.")]
        [Display(Name = "Senha: ")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [Display(Name ="Lembrar Me")]
        public bool LembrarMe { get; set; }
    }
}