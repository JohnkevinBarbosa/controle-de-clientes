﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mobweb.web.Models
{
    public class Usuario
    {
        public int UsuarioID { get; set; }

        [Required(ErrorMessage = "Digite seu nome de usuario.")]
        [Display(Name = "Nome de usuario:")]
        [RegularExpression(@"^\w*", ErrorMessage = "o nome de usuario não deve conter character especiais.")]
        public string NomeUsuario { get; set; }

        [Required(ErrorMessage = "Digite sua senha.")]
        [Display(Name = "Senha:")]
        [RegularExpression(@"^\w*$", ErrorMessage ="Senha não deve conter character especiais.")]
        public string Senha { get; set; }

        [Required(ErrorMessage = "Digite seu nome completo")]
        [Display(Name="Nome Completo:")]
        [RegularExpression(@"^[A-z' ']*$",ErrorMessage ="Nome Invalido")]
        public string NomeCompleto { get; set; }

        [Display(Name = "E-mail:")]
        [RegularExpression(@"^\w*(\.\w*)?@\w*\.[a-z]+(\.[a-z]+)?$",ErrorMessage ="Email Invalido")]
        public string Email { get; set; }

        [Display(Name = "Telefone:")]
        public string Telefone { get; set; }

        public virtual ICollection<Cliente> Clientes { get; set; }

    }
}