﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Mobweb.web.Models
{
    public class Cliente
    {
        public int ClienteID { get; set; }
        [Display(Name="Razão Social:")]
        [Required]
        public string RazaoSocial { get; set; }
        [Display(Name = "Nome Fantasia:")]
        [Required]
        public string NomeFantasia { get; set; }
        [Required]
        [MinLength(18,ErrorMessage = "CNPJ deve conter pelo menos 14 caracteres")]
        public string CNPJ { get; set; }
        public string Logradouro { get; set; }
        public int Numero { get; set; }
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        [Required]
        public string Municio { get; set; }
        [Required]
        public string Cep { get; set; }

        public int UsuarioID { get; set;}

        [ForeignKey("UsuarioID")]
        public virtual Usuario usuario { get; set; }

    }
}