﻿using Mobweb.web.Helpers;
using Mobweb.web.Models;
using Mobweb.web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mobweb.web.Controllers
{
    public class ClienteController : Controller
    {
        ClienteService clienteService = new ClienteService();
        UsuarioService userService = new UsuarioService();


        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Create([Bind(Include = "ClienteID,RazaoSocial,NomeFantasia,CNPJ,Logradouro,Numero,Bairro,Complemento,Municio,Cep,UsuarioID")] Cliente client)
        {
            if (clienteService.ValidarCNPJ(client.CNPJ))
            {
                int usuarioID = userService.GetUsuarioByLogin(User.Identity.Name).UsuarioID;
                client.UsuarioID = usuarioID;
                clienteService.CreateClient(client);
                return RedirectToAction("Clients", "Usuario");
            }
            else
            {
                ModelState.AddModelError("", "CNPJ invalido.");
                return View();
            }
           
        }

        [Authorize]
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario user = userService.GetUsuarioByLogin(User.Identity.Name);
            Cliente client = clienteService.GetClientByID((int)id);
            if(client.UsuarioID!=user.UsuarioID)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            return View(client);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Details([Bind(Include = "ClienteID,RazaoSocial,NomeFantasia,CNPJ,Logradouro,Numero,Bairro,Complemento,Municio,Cep,UsuarioID")] Cliente cliente)
        {
            clienteService.Update(cliente);
            return View(cliente);
        }

        [Authorize]
        public ActionResult Remove(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario user = userService.GetUsuarioByLogin(User.Identity.Name);
            Cliente client = clienteService.GetClientByID((int)id);
            if (client.UsuarioID != user.UsuarioID)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            clienteService.Remove(client.ClienteID);
            return RedirectToAction("Clients", "Usuario");
        }

        [Authorize]
        public string CheckCNPJ(string cnpj)
        {
            if (MasterValidator.ValidarCNPJ(cnpj))
            {
                if (clienteService.ExistCNPJ(cnpj))
                    return "CNPJ já cadastrado no sistema.";
                else
                    return "CNPJ valido.";
            }
            return "CNPJ invalido.";
        }

        [Authorize]
        public string CheckCNPJWithID(string cnpj,int clienteID)
        {
            if (MasterValidator.ValidarCNPJ(cnpj))
            {
                if (clienteService.ExistCNPJWithID(cnpj,clienteID))
                    return "CNPJ já cadastrado no sistema.";
                else
                    return "CNPJ valido.";
            }
            return "CNPJ invalido.";
        }

    }
}