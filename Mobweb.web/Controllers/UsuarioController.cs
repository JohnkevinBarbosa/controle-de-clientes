﻿using Mobweb.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Mobweb.web.Helpers;
using Mobweb.web.Services;
using System.Net;
using Mobweb.web.DAL;
using System.Data.Entity;

namespace Mobweb.web.Controllers
{
    public class UsuarioController : Controller
    {

        private UsuarioService userService = new UsuarioService();
       [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(ContaViewModel usuario,string returnUrl)
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View(usuario);
            }
            
            var achou = userService.GetLogin(usuario);
            //achou = true;
            
            if (achou!=null)
            {
                FormsAuthentication.SetAuthCookie(usuario.UserName, usuario.LembrarMe);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Login e/ou senha Invalidos!");
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult SignIn()
        {
            if (Request.IsAuthenticated)
            {
               return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [Authorize]
        public ActionResult Details()
        {
            Usuario user = userService.GetUsuarioByLogin(User.Identity.Name);
         
                if(user==null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            else
            {
                return View(user);
            }
                
        }

        [HttpPost]
        [Authorize]
        public ActionResult Details([Bind(Include = "UsuarioID,NomeUsuario,Senha,NomeCompleto,Email,Telefone")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
               Usuario userTemp = userService.GetUsuarioByID(usuario.UsuarioID);

               userService.Update(usuario);
               FormsAuthentication.SignOut();
               FormsAuthentication.SetAuthCookie(usuario.NomeUsuario, false);

            }
            return View(usuario);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SignIn([Bind(Include = "UsuarioID,NomeUsuario,Senha,NomeCompleto,Email,Telefone")] Usuario usuario)
        {
            switch (userService.CreateUsuario(usuario))
            {
                case 2:
                    ModelState.AddModelError("", "Usuario ja existe no sistema");
                    break;
                case 3:
                    ModelState.AddModelError("", "Email ja cadastrado.");
                    break;
                case 1:
                    return RedirectToAction("Login");
                    
            }
            return View();
        }

        public ActionResult Clients()
        {
            Usuario user = userService.GetUsuarioByLogin(User.Identity.Name);
            if (user != null)
            {
                List<Cliente> lista = userService.GetClientesByUserID(user.UsuarioID);
                if (lista == null)
                    lista = new List<Cliente>();
                return View(lista);
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}
